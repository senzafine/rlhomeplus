<?php get_template_part('views/header'); ?>

<?php get_template_part('views/partials/about'); ?>
<?php get_template_part('views/partials/projects'); ?>
<?php get_template_part('views/partials/testimonials'); ?>
<?php get_template_part('views/partials/contact'); ?>

<?php get_template_part('views/footer'); ?>