<?php
define('WP_USE_THEMES', true);
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

if (isset($_POST['name']) and ! empty($_POST['name'])
    and isset($_POST['email']) and ! empty($_POST['email'])
    and isset($_POST['phone']) and ! empty($_POST['phone'])
    and isset($_POST['subject']) and ! empty($_POST['subject'])
    and isset($_POST['message']) and ! empty($_POST['message'])
    ) {
    $name    = sanitize_text_field($_POST['name']);
    $email   = sanitize_email($_POST['email']);
    $phone   = $_POST['phone'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $to      = get_option('contact_email_address');
    $subject = __('Email for', 'spiral') .' '. get_bloginfo('name');
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    $owl = wp_mail($to, $subject, strip_tags($message), $headers);

    if ($owl) {
        echo __("You're message has been sent.", 'spiral');
    } else {
        echo __("Sorry, there's been an error submitting the form. Please try again.", 'spiral');
    }
    exit();
} else {
    echo __('Please fill all fields and try again', 'spiral');
    exit();
}
?>