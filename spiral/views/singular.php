@extends('layouts.app')

@section('content')

  <?php while(have_posts(): ?>
    <?php the_post(); ?>
    <article <?php post_class('entry-single'); ?>>
      <?php get_template_part('partials/content-header'); ?>

      <div class="entry-body">
        <?php spiral_sharing(); ?>
        <?php get_template_part('partials.content-' . get_post_type() .''); ?>
      </div>

      <?php if (has_tag() || is_page() || spiral_is_paginated(): ?>
        <footer class="entry-footer">
          <?php if (has_tag()): ?>
            <div class="entry-tags">
              <?php get_the_tag_list('', '', ''); ?>
            </div>
          <?php endif; ?>

          <?php if (is_paged() || $paged || spiral_is_paginated()) : ?>
            <?php spiral_post_pagination(); ?>
          <?php endif; ?>
        </footer>
      <?php endif; ?>

      <?php if (comments_open() || pings_open() || have_comments()): ?>
        <section id="discussion">
          <?php comments_template(); ?>
        </section>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>

@endsection
