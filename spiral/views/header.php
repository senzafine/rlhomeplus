<!doctype html>
<html <?php language_attributes(); ?> xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">

  <?php get_template_part('views/partials/head'); ?>

  <body id="home" <?php body_class(); ?>>

    <?php spiral_google_analytics(); ?>
    <?php spiral_facebook_sdk(); ?>

    <!--[if IE]>
      <div class="alert alert-warning">
        <?= __('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'spiral'); ?>
      </div>
    <![endif]-->

    <?php get_template_part('views/partials/header'); ?>

    <div role="document" class="container">
      <div id="content">

        <main role="main">
          <div class="wrapper">