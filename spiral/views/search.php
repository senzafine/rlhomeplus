@extends('layouts.app')

@section('content')

  @include('partials.content-header')
  @include('partials.searchform')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{  __("These aren't the droids you're looking for.", 'spiral') }}
    </div>
  @else
    <section class="entry-list">
      @while(have_posts())
        @php(the_post())
        @include ('partials.summary-search')
      @endwhile
    </section>
    {!! spiral_posts_pagination() !!}
  @endif

@endsection
