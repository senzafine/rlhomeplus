{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  <?php while(have_posts(): ?>
    <?php the_post(); ?>
    <?php get_template_part('partials/content-header'); ?>
    <?php get_template_part('partials/content-page'); ?>
  <?php endwhile; ?>
@endsection
