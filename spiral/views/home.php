@extends('layouts.app')

@section('content')

  @include('home.about')
  @include('home.projects')
  @include('home.testimonials')
  @include('home.contact')

@endsection
