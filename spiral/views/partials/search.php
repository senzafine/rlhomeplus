<div id="header-search">
  <input type="checkbox" id="toggle-search" class="toggle-checkbox">
  <label for="toggle-search" aria-label="{{ __('Search', 'spiral') }}" id="toggle-search-button" class="toggle-label toggle-search">
    <svg aria-hidden="true" class="icon icon-search">
      <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#search"/>
    </svg>
  </label>
  @include('partials.searchform')
</div>
