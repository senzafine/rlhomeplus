<?php
if (post_password_required()) {
  return;
}
?>

<?php if (comments_open() || pings_open() || have_comments()) : ?>
  <section id="discussion">
    <?php if (have_comments()) : ?>
      <h1><?= spiral_comments_number_text(); ?></h1>
      <ol class="comments-list">
        <?php
        wp_list_comments( array(
          'type'     => 'comment',
          'callback' => 'spiral_comment'
        ));
        ?>
      </ol>

      <?php if (pings_open() === true) : ?>
        <ol class="pingbacks-list">
          <?php
          wp_list_comments( array(
            'type' => 'pings',
            'callback' => 'spiral_pingback'
          ));
          ?>
        </ol>
      <?php endif; ?>

      <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
        <nav>
          <ul class="comments-pagination">
            <?php if (get_previous_comments_link()) : ?>
              <li class="comments-prev">
                <svg class="icon">
                  <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#prev"/>
                </svg>
                <?php previous_comments_link(__('Older Comments', 'spiral')); ?>
              </li>
            <?php endif; ?>

            <?php if (get_next_comments_link()) : ?>
              <li class="comments-prev">
                <svg class="icon">
                  <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#next"/>
                </svg>
                <?php next_comments_link(__('Newer Comments', 'spiral')); ?>
              </li>
            <?php endif; ?>
          </ul>
        </nav>
      <?php endif; ?>
    <?php endif; ?>

    <?php if (!comments_open() && '0' != get_comments_number() && post_type_supports(get_post_type(), 'comments')) : ?>
      <div class="alert alert-warning">
        <?= __('Comments are closed.', 'spiral'); ?>
      </div>
    <?php endif; ?>

    <?php comment_form(spiral_comment_form()); ?>
  </section>
<?php endif; ?>
