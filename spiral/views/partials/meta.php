<div class="meta">
  @if (is_singular())
    @if ('post' === get_post_type() || 'editorial' === get_post_type() || 'review' === get_post_type())
      <div class="meta-time">
        {{ __('Date', 'spiral') }} <time datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
      </div>
    @endif

    @if ('gallery' === get_post_type() || 'page' === get_post_type())
      <div class="meta-time">
        {{ __('Updated', 'spiral') }} <time datetime="{{ get_post_time('c', true) }}">{{ get_the_modified_date() }}</time>
      </div>
    @endif

    @if ('post' === get_post_type())
      <div class="meta-categories">
        {{ __('Category', 'spiral') }} {!! get_the_category_list(' , ') !!}
      </div>
    @endif

    @if ('gallery' !== get_post_type() && 'page' !== get_post_type())
      <div class="meta-author">
        {{ __('Author', 'spiral') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author">{{ get_the_author() }}</a>
      </div>
    @endif
  @else
    <div class="meta-time">
      @if ('gallery' !== get_post_type())
        <time datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
      @else
        <time datetime="{{ get_post_modified_time('c', true) }}">{{ __('Updated', 'spiral') }} {{ get_the_modified_date() }}</time>
      @endif
    </div>
  @endif
</div>
