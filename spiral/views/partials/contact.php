<section class="box" id="contact">
  <h1><?= __('Contact', 'spiral'); ?></h1>

  <form action="<?= get_stylesheet_directory_uri(); ?>/contact.php" id="contact-form">
    <div class="form-group">
      <input type="text" name="name" placeholder="Name">
      <input type="email" name="email" placeholder="Email">
    </div>
    <div class="form-group">
      <input type="phone" name="phone" placeholder="Phone">
      <input type="text" name="subject" placeholder="Subject">
    </div>
    <div class="form-group">
      <textarea name="message" placeholder="How can we help?"></textarea>
    </div>

    <input type="submit" value="<?= __('Send request', 'spiral'); ?>" id="contact-submit">
  </form>

  <div class="alert alert-info" id="contact-message"></div>
</section>
