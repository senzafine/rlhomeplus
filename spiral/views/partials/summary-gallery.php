<article {{ post_class('summary') }}>
  @if (has_post_thumbnail())
    <figure class="summary-thumbnail">
      <a href="{{ the_permalink() }}">
        {!! get_the_post_thumbnail($post->ID, 'thumbnail') !!}
      </a>
    </figure>
  @endif
  <div class="summary-body">
    <header class="summary-header">
      <h1><a href="{{ the_permalink() }}">{{ the_title() }}</a></h1>
      @include('partials/meta')
    </header>
  </div>
</article>
