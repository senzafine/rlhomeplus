<ul class="social-links">
  <li>
    <a href="https://twitter.com/moviefy">
      <svg aria-hidden="true" class="icon icon-twitter">
        <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#twitter"/>
      </svg>
    </a>
  </li>
  <li>
    <a href="https://www.facebook.com/moviefy/">
      <svg aria-hidden="true" class="icon icon-facebook">
        <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#facebook"/>
      </svg>
    </a>
  </li>
  <li>
    <a href="https://plus.google.com/u/1/b/112456790953195957056/+moviefyNet">
      <svg aria-hidden="true" class="icon icon-google">
        <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#google"/>
      </svg>
    </a>
  </li>
  <li>
    <a href="http://pinterest.com/moviefy">
      <svg aria-hidden="true" class="icon icon-pinterest">
        <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#pinterest"/>
      </svg>
    </a>
  </li>
</ul>
