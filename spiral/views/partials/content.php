<?php

if (is_singular()) {
  get_template_part('views/partials/singular');
} elseif (is_archive()) {
  get_template_part('views/partials/archive');
}