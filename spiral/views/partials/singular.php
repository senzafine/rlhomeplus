<?php if (have_posts()) : the_post(); ?>
  <h1><?php the_title(); ?></h1>
  
  <?php the_content(); ?>
  
  <?php spiral_sharing(); ?>

<?php endif; ?>