        </div>
      </main>

      <?php get_template_part('views/partials/sidebar'); ?>

    </div>
  </div>
  
  <footer class="footer-main">
    <section class="address-footer">
      <div class="wrapper">
        <p class="address-message"><?= __('Call now', 'spiral'); ?></p>
        <p class="address-phone"><a href="tel:<?= get_option('contact_phone_number'); ?>"><?= get_option('contact_phone_number'); ?></a></p>
        <p class="address-location"><?= __('15886 Cliffbrook Ct. Dumfries', 'spiral'); ?></p>
      </div>
    </section>
    
    <div class="wrapper">
      <section id="footer-links">
        <?php
        /*
        wp_nav_menu(array(
          'menu'           => __('Footer Menu', 'spiral'),
          'depth'          => 1,
          'theme_location' => 'footer_menu',
          'items_wrap'     => '<nav id="footer-menu"><ul>%3$s</ul></nav>'
        ));
        */
        ?>
      </section>
      
      <?php dynamic_sidebar('sidebar-footer'); ?>
      
      <div class="credits">
        <div class="credits-logo">
          <svg aria-hidden="true">
            <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#logo"/>
          </svg>
        </div>
      </div>
    </div>
  </footer>
  
  <?php wp_footer(); ?>

</body>
</html>