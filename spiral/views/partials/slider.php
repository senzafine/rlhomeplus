<section class="swiper-frontpage">
  <div class="swiper-wrapper">
    <?php
    $slides = new WP_Query(array(
      'numberposts' => -1,
      'post_type' => 'slide'
    ));

    if ($slides->have_posts()) {
      while ($slides->have_posts()) {
        $slides->the_post();
        ?>
        <figure class="swiper-slide">
          <a href="#"><img src="<?php the_post_thumbnail_url('full'); ?>"></a>
          <figcaption><?php the_title(); ?></figcaption>
        </figure>
        <?php
      }
    }

    wp_reset_postdata();
    ?>
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-next">
    <svg aria-hidden="true" class="icon icon-search">
      <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#next"/>
    </svg>
  </div>
  <div class="swiper-button-prev">
    <svg aria-hidden="true" class="icon icon-search">
      <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#prev"/>
    </svg>
  </div>
</section>
