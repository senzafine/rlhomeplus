<section class="box" id="about">
  <h1><?= __('About', 'spiral'); ?></h1>
  <p><?= get_option('about_intro'); ?></p>
  <div class="panel-group">
    <div class="panel">
      <figure>
        <svg class="icon" aria-hidden="true">
          <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#tool-1"/>
        </svg>
      </figure>
      <p><?= get_option('about_panel_1'); ?></p>
    </div>
    <div class="panel">
      <figure>
        <svg class="icon" aria-hidden="true">
          <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#tool-14"/>
        </svg>
      </figure>
      <p><?= get_option('about_panel_2'); ?></p>
    </div>
    <div class="panel">
      <figure>
        <svg class="icon" aria-hidden="true">
          <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#tool-12"/>
        </svg>
      </figure>
      <p><?= get_option('about_panel_3'); ?></p>
    </div>
    <div class="panel">
      <figure>
        <svg class="icon" aria-hidden="true">
          <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#tool-13"/>
        </svg>
      </figure>
      <p><?= get_option('about_panel_4'); ?></p>
    </div>
  </div>
</section>
