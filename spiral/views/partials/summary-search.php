<article {{ post_class('summary') }}>
  @if (has_post_thumbnail())
    <figure class="summary-thumbnail">
      <a href="{{ the_permalink() }}">
        {!! get_the_post_thumbnail($post->ID, 'thumbnail') !!}
      </a>
    </figure>
  @endif
  <div class="summary-body">
    <header class="summary-header">
      @php($title = get_the_title())
      @if ('review' === get_post_type() && !is_archive('review'))
        @php($title = $title . ' <span class="post-type">' . __('Review', 'spiral') . '</span>')
      @elseif ('gallery' === get_post_type() && !is_archive('gallery'))
        @php($title = $title . ' <span class="post-type">' . __('Gallery', 'spiral') . '</span>')
      @endif
      <h1><a href="{{ the_permalink() }}">{!! $title !!}</a></h1>
    </header>
  </div>
</article>
