<form role="search" method="get" action="{!! esc_url(home_url('/')) !!}" class="search-form">
  <label for="s" class="screen-reader-text">{{ __('Search', 'spiral') }}</label>
  <input type="search" name="s" value="{!! the_search_query() !!}" placeholder="{{ __('Search', 'spiral') }}" id="s" class="search-field">
  <button class="search-submit">
    <svg aria-hidden="true" class="icon icon-search">
      <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#search"/>
    </svg>
    <svg aria-hidden="true" class="icon icon-close">
      <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#close"/>
    </svg>
  </button>
</form>
