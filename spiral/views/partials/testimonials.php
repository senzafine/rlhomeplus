<section class="box" id="testimonials">
  <h1><?= __('Testimonials', 'spiral'); ?></h1>

  <div class="swiper-testimonial">
    <div class="swiper-wrapper">

      <?php
      $args = array(
        'numberposts' => -1,
        'post_type' => 'testimonial'
      );

      $testimonials = new WP_Query($args);

      if ($testimonials->have_posts()) {
        while ($testimonials->have_posts()) {
          $testimonials->the_post();
          ?>
          <div class="swiper-slide">
            <blockquote>
              <?= get_the_content(); ?>
              <cite><?= get_the_title(); ?></cite>
            </blockquote>
          </div>
          <?php
        }
      }

      wp_reset_postdata();
      ?>

    </div>
    <div class="swiper-button-next">
      <svg aria-hidden="true" class="icon icon-search">
        <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#next"/>
      </svg>
    </div>
    <div class="swiper-button-prev">
      <svg aria-hidden="true" class="icon icon-search">
        <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#prev"/>
      </svg>
    </div>
  </div>
</section>
