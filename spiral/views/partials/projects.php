<section class="box" id="projects">
  <h1><?= __('Projects', 'spiral'); ?></h1>
  <div class="project-group">
    
    <?php
    $projects = new WP_Query(array(
      'posts_per_page' => 2,
      'post_type' => 'project'
    ));

    if ($projects->have_posts()) {
      while ($projects->have_posts()) {
        $projects->the_post();
        $post_id = $post->ID;
        $project_before_id = get_post_meta($post_id, 'project-before-id', true);
        $project_after_id = get_post_meta($post_id, 'project-after-id', true);
        $project_before_thumbnail = wp_get_attachment_image_src($project_before_id)[0];
        $project_before_image = wp_get_attachment_image_src($project_before_id, 'full')[0];
        $project_after_thumbnail = wp_get_attachment_image_src($project_after_id)[0];
        $project_after_image = wp_get_attachment_image_src($project_after_id, 'full')[0];
        ?>
        <div class="project">
          <div class="project-photos">
            <figure>
              <div class="project-photo">
                <a href="<?= $project_before_image; ?>"><img src="<?= $project_before_thumbnail; ?>"></a>
              </div>
              <figcaption><?= __('Before', 'spiral'); ?></figcaption>
            </figure>
            <figure>
              <div class="project-photo">
                <a href="<?= $project_after_image; ?>"><img src="<?= $project_after_thumbnail; ?>"></a>
              </div>
              <figcaption><?= __('After', 'spiral'); ?></figcaption>
            </figure>
          </div>
          <p><?= get_the_excerpt(); ?></p>
        </div>
        <?php
      }
    }

    wp_reset_postdata();
    ?>
  </div>

  <div class="button">
    <a href="<?= esc_url(home_url('/')); ?>/projects"><?= __('View more', 'spiral'); ?></a>
  </div>
</section>
