<section class="menu-main">
  <div class="wrapper">
    <h1 class="brand brand-small">
      <a href="#" aria-label="<?php bloginfo('name'); ?>">
        <svg aria-hidden="true">
          <title><?php bloginfo('name'); ?></title>
          <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#logo"/>
        </svg>
      </a>
    </h1>

    <input type="checkbox" id="toggle-menu" class="toggle-checkbox">
    <label for="toggle-menu" aria-label="<?= __('Menu', 'spiral'); ?>" class="toggle-label toggle-menu">
      <svg aria-hidden="true" class="icon icon-menu">
        <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#menu"/>
      </svg>
      <svg aria-hidden="true" class="icon icon-close">
        <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#close"/>
      </svg>
    </label>

    <?php
    wp_nav_menu( array(
      'items_wrap'      => '<nav><ul>%3$s</ul></nav>',
      'menu'            => __('Main Menu', 'spiral'),
      'theme_location'  => 'main_menu'
    ));
    ?>

    <div class="button-menu">
      <a href="#">
        <?= __('Get a quote', 'spiral'); ?>
      </a>
    </div>
  </div>
</section>
