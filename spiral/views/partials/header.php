<header class="header-main">
    <div class="admin-bar">
        <?php
        if (is_user_logged_in()) {
            $user = wp_get_current_user();

            echo sprintf('<p>%1$s, %2$s. <a href="%3$s">%4$s</a></p>', __('Welcome', 'spiral'), $user->user_nicename, wp_logout_url(), __('Logout?', 'spiral'));
            echo sprintf('<a href="%1$s" class="button">%2$s</a>', home_url('/wp-admin'), __('Go to Dashboard', 'spiral'));
        } else {
            $args = array(
                'redirect' => home_url() . '/wp-admin',
                'remember' => false,
                'label_username' => __('User', 'spiral'),
                'label_password' => __('Password', 'spiral')
            );
    
            wp_login_form($args);
        }
        ?>
    </div>

    <div class="quote-popup">
        <div class="quote-form">
            <a href="#" class="quote-close">
                <svg aria-hidden="true" class="icon icon-close">
                    <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#close"/>
                </svg>
            </a>

            <?php
            $quote_form = get_option('quote_form');

            if (! empty($quote_form)) {
                echo do_shortcode($quote_form);
            } else {
                echo sprintf('<p>%1$s</p><p>%2$s</p>', __('No form shortcode saved.', 'spiral'), __('Go into the Frontpage settings and add a Ninja Forms shortcode.', 'spiral'));
            }
            ?>

            <div class="quote-message alert">
                <p><?= __('Thank you for your message.', 'spiral'); ?></p>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <h1 class="brand">
            <a href="<?= esc_url(home_url('/')); ?>" aria-label="<?= bloginfo('name'); ?>">
                <svg aria-hidden="true">
                    <title><?= bloginfo('name'); ?></title>
                    <use xlink:href="<?= get_stylesheet_directory_uri(); ?>/assets/images/sprite.svg#logo"/>
                </svg>
            </a>
        </h1>

        <div class="address-header">
            <p class="address-phone"><a href="tel:<?= get_option('contact_phone_number'); ?>"><?= get_option('contact_phone_number'); ?></a></p>
            <p class="address-email"><a href="mailto:<?= get_option('contact_email_address'); ?>"><?= get_option('contact_email_address'); ?></a></p>
        </div>
    </div>

    <?php get_template_part('views/partials/slider'); ?>
    <?php get_template_part('views/partials/menu'); ?>
</header>
