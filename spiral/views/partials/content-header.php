<header class="entry-header">
  <h1><?php title(); ?></h1>

  <?php if (is_single()) : ?>
    <?php get_template_part('partials.meta-'.get_post_type()); ?>
  <?php endif; ?>
</header>
