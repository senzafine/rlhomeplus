<h1><?= title(); ?></h1>

<div class="summary-group">
  
  <?php
  if (have_posts()) {
    while (have_posts()) {
      the_post();
      $post_id = $post->ID;

      if (is_post_type_archive('project')) {
        ?>
        <div class="summary">
          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <figure>
            <a href="<?php the_permalink(); ?>">
              <img src="<?php the_post_thumbnail_url('thumbnail'); ?>">
            </a>
          </figure>
          <p><?php the_excerpt(); ?></p>
        </div>
        <?php
      }
    }
  }
  ?>
</div>