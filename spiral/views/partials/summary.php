<article {{ post_class('summary') }}>
  @if (has_post_thumbnail())
    <figure class="summary-thumbnail">
      <a href="{{ the_permalink() }}">
        {!! get_the_post_thumbnail($post->ID, 'featured_image_thumbnail') !!}
      </a>
    </figure>
  @endif
  <div class="summary-body">
    <header class="summary-header">
      @php($title = get_the_title())
      @if ('review' === get_post_type() && !is_archive('review'))
        @php($title = $title . ' <span class="post-type">' . __('Review', 'spiral') . '</span>')
      @endif
      <h1><a href="{{ the_permalink() }}">{!! $title !!}</a></h1>
      @include('partials/meta')
    </header>
  </div>
</article>
