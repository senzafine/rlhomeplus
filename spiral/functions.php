<?php

/** Spiral required files */
require_once(__DIR__ . '/core/setup.php');
require_once(__DIR__ . '/core/filters.php');
require_once(__DIR__ . '/core/login.php');
require_once(__DIR__ . '/core/dashboard.php');
require_once(__DIR__ . '/core/posttypes.php');
require_once(__DIR__ . '/core/taxonomies.php');
require_once(__DIR__ . '/core/metaboxes.php');
require_once(__DIR__ . '/core/images.php');
require_once(__DIR__ . '/core/titles.php');
require_once(__DIR__ . '/core/posts.php');
require_once(__DIR__ . '/core/social.php');
require_once(__DIR__ . '/core/meta.php');
require_once(__DIR__ . '/core/comments.php');
require_once(__DIR__ . '/core/pagination.php');
require_once(__DIR__ . '/core/codes.php');
