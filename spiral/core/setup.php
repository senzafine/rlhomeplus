<?php

function asset_path($asset) {
    static $manifest = null;
    
    if (null === $manifest) {
        $manifest_path = get_stylesheet_directory() . '/assets/assets.json';
        $manifest = file_exists($manifest_path)
            ? json_decode(file_get_contents($manifest_path), true)
            : [];
    }
    
    if (array_key_exists($asset, $manifest)) {
        return get_stylesheet_directory_uri() .'/assets/' . $manifest[$asset];
    }
 
    return get_stylesheet_directory_uri() .'/assets/'. $asset;
}


/** Frontend assets */
function spiral_frontend_assets ()
{
    wp_deregister_script('jquery');
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('spiral/main.js', asset_path('scripts/main.js'), false, null, true);
}
add_action('wp_enqueue_scripts', 'spiral_frontend_assets', 100);

/** Dashboard assets */
function spiral_admin_assets ()
{
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/admin.css', asset_path('styles/admin.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('suggest');
    wp_enqueue_script('spiral/admin.js', asset_path('scripts/admin.js'), false, null, true);
    wp_localize_script('spiral/admin.js', 'wpajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}
add_action('admin_enqueue_scripts', 'spiral_admin_assets', 100);

/** Login assets */
function spiral_login_assets ()
{
    wp_enqueue_style('spiral/fonts', google_fonts(), false);
    wp_enqueue_style('spiral/vendor.css', asset_path('styles/vendor.css'), false, null);
    wp_enqueue_style('spiral/login.css', asset_path('styles/login.css'), false, null);
    wp_enqueue_script('spiral/manifest.js', asset_path('scripts/manifest.js'), false, null, true);
    wp_enqueue_script('spiral/vendor.js', asset_path('scripts/vendor.js'), false, null, true);
    wp_enqueue_script('spiral/login.js', asset_path('scripts/login.js'), false, null, true);
}
add_action('login_enqueue_scripts', 'spiral_login_assets', 100);

/** Theme setup */
function spiral_setup ()
{
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');
    add_theme_support('title-tag');

    register_nav_menus( array(
        'main_menu'   => __('Main Menu', 'spiral'),
        'footer_menu' => __('Footer Menu', 'spiral')
    ));

    add_theme_support('post-thumbnails');
    add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'));
    add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));
    load_theme_textdomain('spiral', get_template_directory() .'/languages');
    add_theme_support('automatic-feed-links');
    show_admin_bar(false);
    add_theme_support('customize-selective-refresh-widgets');
    add_editor_style(asset_path('styles/editor.css'));

    update_option('permalink_structure', '/%category%/%postname%/');
    update_option('uploads_use_yearmonth_folders', 0);
    update_option('start_of_week', 0);
    update_option('thumbnail_size_w', 0);
    update_option('thumbnail_size_h', 0);
    update_option('medium_size_w', 0);
    update_option('medium_size_h', 0);
    update_option('medium_large_size_w', 0);
    update_option('medium_large_size_h', 0);
    update_option('large_size_w', 0);
    update_option('large_size_h', 0);
}
add_action('after_setup_theme', 'spiral_setup', 20);

/** Register sidebars */
function spiral_widgets ()
{
    register_sidebar( array(
        'name'          => __('Primary', 'spiral'),
        'id'            => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h1>',
        'after_title'   => '</h1>'
    ));

    register_sidebar( array(
        'name'          => __('Footer', 'spiral'),
        'id'            => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h1>',
        'after_title'   => '</h1>'
    ));
}
add_action('widgets_init', 'spiral_widgets');

/** Define Google fonts */
function google_fonts ()
{
    $fonts = '//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Vollkorn:400,400i,600,600i,700,700i,900,900i';
    return $fonts;
}

/** Custom buttons for the visual editor */
function spiral_mce_buttons ($buttons, $editor_id)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'spiral_mce_buttons', 10, 2);

function spiral_tinymce ($settings)
{
    $style_formats = array(
        array(
            'title'   => 'Movie',
            'inline'  => 'cite',
            'classes' => 'movie'
        ),

        array(
            'title'  => 'Figure',
            'block' => 'figure'
        ),

        array(
            'title'  => 'Quote',
            'inline' => 'q'
        ),

        array(
            'title'  => 'Responsive Video',
            'inline' => 'div',
            'class'  => 'embed-video'
        )
    );

    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
}
add_filter('tiny_mce_before_init', 'spiral_tinymce');