<?php

/** Display Social Sharing Links */
function spiral_sharing ()
{
    global $post;

    if (is_singular()) {
        if ('page' !== get_post_type()) {
            $post_url       = get_permalink();
            $post_title     = str_replace(' ', '%20', get_the_title());
            $post_thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'featured_image');

            $twitter   = "https://twitter.com/intent/tweet?text={$post_title}&amp;url={$post_url}&amp;via=moviefy";
            $facebook  = "https://www.facebook.com/sharer/sharer.php?u={$post_url}";
            $google    = "https://plus.google.com/share?url={$post_url}";
            $pinterest = "https://pinterest.com/pin/create/button/?url={$post_url}&amp;media={$post_thumbnail[0]}&amp;description={$post_title}";
            $linkedin  = "https://www.linkedin.com/shareArticle?mini=true&url={$post_url}&amp;title={$post_title}";
            $whatsapp  = "whatsapp://send?text={$post_url}";
            $buffer    = "https://bufferapp.com/add?url={$post_url}&amp;text={$post_title}";

            $output = "
            <aside class='social-aside'>
                <div class='social-text'>". __('Share this:', 'spiral') ."</div>
                <ul class='social-share'>
                    <li>
                        <a href='{$twitter}' target='_blank' aria-label='Twitter'>
                            <svg aria-hidden='true' class='icon icon-twitter'>
                                <use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#twitter_share'/>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href='{$facebook}' target='_blank' aria-label='Facebook'>
                            <svg aria-hidden='true' class='icon icon-facebook'>
                                <use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#facebook_share'/>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href='{$google}' target='_blank' aria-label='Google'>
                            <svg aria-hidden='true' class='icon icon-google'>
                                <use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#google_share'/>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href='{$pinterest}' target='_blank' aria-label='Pinterest'>
                            <svg aria-hidden='true' class='icon icon-pinterest'>
                                <use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#pinterest_share'/>
                            </svg>
                        </a>
                    </li>
                    <li>
                        <a href='{$whatsapp}' target='_blank' aria-label='WhatsApp'>
                            <svg aria-hidden='true' class='icon icon-whatsapp'>
                                <use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#whatsapp_share'/>
                            </svg>
                        </a>
                    </li>
                </ul>
            </aside>";

            echo $output;
        }
    }
}
