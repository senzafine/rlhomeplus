<?php

/** Add metaboxes */
function spiral_before_after_metabox ()
{
  add_meta_box('before-after', __('Before and After Pictures', 'spiral'), 'spiral_before_after_callback', 'project');
}
add_action('add_meta_boxes', 'spiral_before_after_metabox');

/** Define metaboxes */
function spiral_before_after_callback ($post)
{
  $post_id           = $post->ID;
  $project_before_id = get_post_meta($post_id, 'project-before-id', true);
  $project_after_id  = get_post_meta($post_id, 'project-after-id', true);
  
  $project_before_src = wp_get_attachment_image_src($project_before_id)[0];
  $project_after_src  = wp_get_attachment_image_src($project_after_id)[0];
  
  ?>

  <?php wp_nonce_field('spiral-before-after-nonce', 'spiral-before-after-nonce'); ?>
  
  <div class="project-pictures">
    <div class="project-picture">
      <label for "project-before-id"><?= __('Before Picture', 'spiral'); ?></label>
      <figure>
        <img id="project-before-preview" src="<?php if (isset($project_before_src)) : echo $project_before_src; endif; ?>">
      </figure>
      <input type="hidden" name="project-before-id" id="project-before-id" value="<?= $project_before_id; ?>">
      <input type="button" name="project-before-button" id="project-before-button" value="<?= __('Choose or Upload an Image', 'spiral'); ?>" class="button">
    </div>
    
    <div class="project-picture">
      <label for "project-after-id"><?= __('After Picture', 'spiral'); ?></label>
      <figure>
        <img id="project-after-preview" src="<?php if (isset($project_after_src)) : echo $project_after_src; endif; ?>">
      </figure>
      <input type="hidden" name="project-after-id" id="project-after-id" value="<?= $project_after_id; ?>">
      <input type="button" name="project-after-button" id="project-after-button" value="<?= __('Choose or Upload an Image', 'spiral'); ?>" class="button">
    </div>
  </div>
  <?php
}

/** Save data from metaboxes */
function spiral_before_after_save ($post_id)
{
  
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return;
  }
  
  if (! isset($_POST['spiral-before-after-nonce']) || ! wp_verify_nonce($_POST['spiral-before-after-nonce'], 'spiral-before-after-nonce')) {
    return;
  }
  
  if (! current_user_can('edit_posts')) {
    return;
  }
  
  if (isset($_POST['project-before-id'])) {
    update_post_meta($post_id, 'project-before-id', $_POST['project-before-id']);
  }
  
  if (isset($_POST['project-after-id'])) {
    update_post_meta($post_id, 'project-after-id', $_POST['project-after-id']);
  }
}
add_action('save_post', 'spiral_before_after_save');