<?php

/** Build metatags */
function spiral_metatags ()
{
    global $post;

    if (! is_object($post)) {
        return;
    }

    $post = get_post($post->ID);

    if (is_single()) {
        $title = get_the_title();
    } else {
        $title = get_bloginfo('name');
    }

    if (is_single()) {
        $excerpt = get_the_excerpt($post->ID);
    } else {
        $excerpt = get_bloginfo('description');
    }

    if (! is_single() || !has_post_thumbnail($post->ID)) {
        $image = get_stylesheet_directory_uri() .'/images/opengraph_image.jpg';
    } else {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'featured_image', false);
        $image = get_bloginfo('url') . $image[0];
    }

    if (is_single()) {
        $url  = get_the_permalink($post->ID);
        $type = 'article';
    } else {
        $url  = get_bloginfo('url');
        $type = 'website';
    }
    ?>

    <meta name="description" content="<?= $excerpt; ?>">

    <!-- Facebook Metatags -->
    <meta property="fb:admins" content="">
    <meta property="fb:app_id" content="">

    <!-- OpenGraph Metatags -->
    <meta property="og:site_name" content="<?= get_bloginfo('name'); ?>">
    <meta property="og:title" content="<?= $title; ?>">
    <meta property="og:description" content="<?= $excerpt; ?>">
    <meta property="og:image" content="<?= $image; ?>">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="315">
    <meta property="og:url" content="<?= $url; ?>">
    <meta property="og:type" content="<?= $type; ?>">

    <!-- Twitter Card Metatags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="">
    <meta name="twitter:domain" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="<?= $title; ?>">
    <meta name="twitter:description" content="<?= $excerpt; ?>">
    <meta name="twitter:image:src" content="<?= $image; ?>">

    <?php
}
add_action('wp_head', 'spiral_metatags', 2);

/** Disable Jetpack metatags */
add_filter('jetpack_enable_open_graph', '__return_false');

/** Add language attributes */
function spiral_language_attributes ($output)
{
    return $output .' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'spiral_language_attributes', 10, 2);

/** Build JSON-LD */
function spiral_schema ()
{
    global $post;

    if (is_single()) {
        if ('post' == get_post_type()) {
            $type = 'NewsArticle';
        } elseif (get_post_type() == 'editorial') {
            $type = 'Article';
        } elseif (get_post_type() == 'review') {
            $type = 'Review';
        } elseif (get_post_type() == 'soundtrack') {
            $type = 'Album';
        }
    } else {
        $type = 'WebSite';
    }

    $published = get_the_date();
    $updated   = get_the_modified_date();
    $title     = get_the_title();
    $site      = get_bloginfo('name');

    if (! is_single() || ! has_post_thumbnail($post->ID)) {
        $image = get_stylesheet_directory_uri() ."/assets/images/opengraph_image.jpg";
    } else {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'featured_image', false);
        $image = get_bloginfo('url') . $image[0];
    }

    $movie = get_the_terms(get_the_ID(), 'movies');

    $output = '<script type="application/ld+json">{';

    if (is_single()) {
        $output .= '
        "@context" : "http://schema.org",
        "@type"    : "'. $type .'",
        "author"   : {
            "@type" : "Person",
            "name"  : "",
            "sameAs": ""
        },
        "datePublished": "'. $published .'",
        "dateModified" : "'. $updated .'",
        "headline"     : "'. $title .'",
        "description"  : "'. get_the_excerpt() .'",
        "image": {
            "@type" : "ImageObject",
            "url"   : "'. $image .'",
            "width" : "600",
            "height": "315"
        },';

        $output .= '
        "publisher": {
            "@type": "Organization",
            "name" : "'. $site .'",
            "logo": {
                "@type": "ImageObject",
                "url"  : "'. get_stylesheet_directory_uri() .'/assets/images/logo.png"
            }
        },
        "mainEntityOfPage": {
            "@type": "WebPage",
            "@id"  : "'. get_bloginfo('url') .'"
        }';
    } else {
        $output .= '
        "@context" : "http://schema.org",
        "@type"    : "'. $type .'",
        "@id"      : "'. get_bloginfo('url') .'"';
    }

    $output .= '
        }
    </script>';

    echo $output;
}
