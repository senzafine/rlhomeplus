<?php

/** Change footer text */
function spiral_admin_footer_text () {
    echo "Made by <a href='http://senzafine.net'>Senzafine</a>. Powered by <a href='http://wordpres.org'>WordPress</a>.";
}
add_filter('admin_footer_text', 'spiral_admin_footer_text');

/** Replace Admin Logo */
function spiral_admin_logo () {
    echo "<style type='text/css'>#header-logo {background-image: url(". get_bloginfo('template_directory') ."/images/logo.svg) !important;}</style>";
}
add_action('admin_head', 'spiral_admin_logo');

/** Delays RSS Publishing Feed */
function spiral_delay_rss ($where)
{
    global $wpdb;

    if (is_feed()) {
        $now    = gmdate('Y-m-d H:i:s');
        $wait   = '15';
        $device = 'MINUTE';
        $where  .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
    }

    return $where;
}
add_filter('posts_where', 'spiral_delay_rss');

/** Define custom settings page */
function spiral_settings_page ()
{
  add_options_page(__('Frontpage Settings', 'spiral'), __('Frontpage Settings', 'spiral'), 'manage_options', 'spiral-settings', 'spiral_settings_page_callback');
}
add_action('admin_menu', 'spiral_settings_page');

function spiral_settings_page_callback ()
{
  ?>
  <div class="wrap spiral-settings">
    <h1><?= __('Frontpage Settings', 'spiral'); ?></h1>

    <hr>

    <form method="post" action="options.php">
      <?php settings_fields('spiral-settings'); ?>
      <?php do_settings_sections('spiral-settings'); ?>
      <?php submit_button(); ?>
    </form>
  </div>
  <?php
}

/** Define custom settings sections */
function spiral_settings()
{
  add_settings_section('spiral_settings_quote', __('Quote Form Settings'), 'spiral_settings_quote_callback', 'spiral-settings');
  add_settings_section('spiral_settings_contact', __('Contact Settings'), 'spiral_settings_contact_callback', 'spiral-settings');
  add_settings_section('spiral_settings_about', __('About Us Panels'), 'spiral_settings_about_callback', 'spiral-settings');

  add_settings_field('quote_form', __('Form Shortcode', 'spiral'), 'spiral_quote_form_callback', 'spiral-settings', 'spiral_settings_quote');
  add_settings_field('contact_phone_number', __('Phone Number', 'spiral'), 'spiral_contact_phone_callback', 'spiral-settings', 'spiral_settings_contact');
  add_settings_field('contact_email_address', __('Email Address', 'spiral'), 'spiral_contact_email_callback', 'spiral-settings', 'spiral_settings_contact');
  add_settings_field('about_intro', __('Intro Text', 'spiral'), 'spiral_about_intro_callback', 'spiral-settings', 'spiral_settings_about');
  add_settings_field('about_panel_1', __('Panel 1', 'spiral'), 'spiral_about_panel_1_callback', 'spiral-settings', 'spiral_settings_about');
  add_settings_field('about_panel_2', __('Panel 2', 'spiral'), 'spiral_about_panel_2_callback', 'spiral-settings', 'spiral_settings_about');
  add_settings_field('about_panel_3', __('Panel 3', 'spiral'), 'spiral_about_panel_3_callback', 'spiral-settings', 'spiral_settings_about');
  add_settings_field('about_panel_4', __('Panel 4', 'spiral'), 'spiral_about_panel_4_callback', 'spiral-settings', 'spiral_settings_about');

  register_setting('spiral-settings', 'quote_form');
  register_setting('spiral-settings', 'contact_phone_number');
  register_setting('spiral-settings', 'contact_email_address');
  register_setting('spiral-settings', 'about_intro');
  register_setting('spiral-settings', 'about_panel_1');
  register_setting('spiral-settings', 'about_panel_2');
  register_setting('spiral-settings', 'about_panel_3');
  register_setting('spiral-settings', 'about_panel_4');
}
add_action('admin_init', 'spiral_settings');

function spiral_settings_quote_callback ()
{
  echo '<hr>';
  echo __('<p>Add the Ninja Forms shortcode to display the quote form.</p>', 'spiral');
}

function spiral_quote_form_callback ()
{
  echo '<input type="text" name="quote_form" id="quote_form" value="'. get_option('quote_form') .'" placeholder="[ninja_form id=1]">';
}

function spiral_settings_contact_callback ()
{
  echo '<hr>';
  echo __('<p>Add the contact phone and email address to be displayed on the header and footer.</p>', 'spiral');
}

function spiral_contact_phone_callback ()
{
  echo '<input type="text" name="contact_phone_number" id="contact_phone_number" value="'. get_option('contact_phone_number') .'">';
}

function spiral_contact_email_callback ()
{
  echo '<input type="email" name="contact_email_address" id="contact_email_address" value="'. get_option('contact_email_address') .'">';
}

function spiral_settings_about_callback ()
{
  echo '<hr>';
  echo __('<p>Write the text to be displayed on the <strong>About Us</strong> section of the front page.</p>', 'spiral');
}

function spiral_about_intro_callback ()
{
  echo '<textarea name="about_intro" id="about_intro">'. get_option('about_intro') .'</textarea>';
}

function spiral_about_panel_1_callback ()
{
  echo '<textarea name="about_panel_1" id="about_panel_1">'. get_option('about_panel_1') .'</textarea>';
}

function spiral_about_panel_2_callback ()
{
  echo '<textarea name="about_panel_2" id="about_panel_2">'. get_option('about_panel_2') .'</textarea>';
}

function spiral_about_panel_3_callback ()
{
  echo '<textarea name="about_panel_3" id="about_panel_3">'. get_option('about_panel_3') .'</textarea>';
}

function spiral_about_panel_4_callback ()
{
  echo '<textarea name="about_panel_4" id="about_panel_4">'. get_option('about_panel_4') .'</textarea>';
}
