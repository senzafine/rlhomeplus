<?php

/** Remove default image sizes */
function spiral_image_sizes ($sizes)
{
    unset($sizes['medium']);
    unset($sizes['medium_large']);
    unset($sizes['large']);

    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'spiral_image_sizes');

/** Define default image sizes */
add_image_size('thumbnail', 240, 240, array('center', 'top'));
add_image_size('featured_image_thumbnail', 480, 240, array('center', 'top'));
add_image_size('featured_image', 960, 480, array('center', 'top'));
add_image_size('cover_image', 1200, 400, array('center', 'top'));

/** Define names for custom image sizes */
function spiral_image_size_names ($sizes)
{
    return array_merge($sizes, array(
        'thumbnail'                => __('Thumbnail', 'spiral'),
        'featured_image_thumbnail' => __('Featured Thumbnail', 'spiral'),
        'featured_image'           => __('Featured', 'spiral'),
        'cover_image'              => __('Cover', 'spiral'),
    ));
}
add_filter('image_size_names_choose', 'spiral_image_size_names');

/** Force image resize even if the requested size is bigger than the image */
function spiral_image_resize ($default, $orig_w, $orig_h, $new_w, $new_h, $crop)
{
    if (! $crop) {
        return null;
    }

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio   = max($new_w / $orig_w, $new_h / $orig_h);
    $crop_w       = round($new_w / $size_ratio);
    $crop_h       = round($new_h / $size_ratio);
    $s_x          = floor(($orig_w - $crop_w) / 2);
    $s_y          = floor(($orig_h - $crop_h) / 2);

    return array(0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h);
}
add_filter('image_resize_dimensions', 'spiral_image_resize', 10, 6);

/** Clean Media Library output */
function spiral_clean_caption_shortcode ($attr, $content = null)
{
    if (! isset($attr['caption'])) {
        if (preg_match('#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches)) {
            $content         = $matches[1];
            $attr['caption'] = trim($matches[2]);
        }
    }

    $output = apply_filters('img_caption_shortcode', '', $attr, $content);

    if (! empty($output)) {
        return $output;
    }

    extract(shortcode_atts( array(
        'id'      => '',
        'align'   => 'alignnone',
        'width'   => '',
        'caption' => ''
    ), $attr));

    if (1 > (int) $width || empty($caption)) {
        return $content;
    }

    if ($id) {
        $id = "id='". esc_attr($id) ."'";
    }

    $output = "<figure {$id}>". do_shortcode($content) ."<figcaption>{$caption}</figcaption></figure>";

    return $output;
}
add_shortcode('wp_caption', 'spiral_clean_caption_shortcode');
add_shortcode('caption', 'spiral_clean_caption_shortcode');

/** Modify the gallery shortcode output */
function spiral_gallery_output ($output, $attr)
{
    global $post, $wp_locale;

    $html5 = current_theme_supports('html5', 'gallery');

    static $instance = 0;
    $instance++;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);

        if (! $attr['orderby']) {
            unset($attr['orderby']);
        }
    }

    extract(shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post ? $post->ID : 0,
        'itemtag'    => $html5 ? 'figure' : 'dl',
        'icontag'    => $html5 ? 'div' : 'dt',
        'captiontag' => $html5 ? 'figcaption' : 'dd',
        'columns'    => 5,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => '',
        'link'       => ''
    ), $attr, 'gallery'));

    $id = intval($id);

    if ('RAND' == $order) {
        $orderby = 'none';
    }

    if (! empty($include)) {
        $_attachments = get_posts( array(
            'include'        => $include,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'order'          => $order,
            'orderby'        => $orderby
        ));

        $attachments = array();

        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif (! empty($exclude)) {
        $attachments = get_children( array(
            'post_parent'    => $id,
            'exclude'        => $exclude,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'order'          => $order,
            'orderby'        => $orderby
        ));
    } else {
        $attachments = get_children( array(
            'post_parent'    => $id,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'order'          => $order,
            'orderby'        => $orderby
        ));
    }

    if (empty($attachments)) {
        return '';
    }

    if (is_feed()) {
        $output = "\n";

        foreach ($attachments as $att_id => $attachment) {
            $output .= wp_get_attachment_link($att_id, $size, true) ."\n";
        }

        return $output;
    }

    $itemtag    = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $icontag    = tag_escape($icontag);
    $valid_tags = wp_kses_allowed_html('post');

    if (! isset($valid_tags[$itemtag])) {
        $itemtag = 'dl';
    }

    if (! isset($valid_tags[$captiontag])) {
        $captiontag = 'dd';
    }

    if (! isset($valid_tags[$icontag])) {
        $icontag = 'dt';
    }

    $columns       = intval($columns);
    $itemwidth     = $columns > 0 ? floor(100/$columns) : 100;
    $float         = is_rtl() ? 'right' : 'left';
    $selector      = "gallery-{$instance}";
    $size_class    = sanitize_html_class($size);

    $output = "<div id='{$selector}' class='gallery gallery-group gallery-columns-{$columns} gallery-size-{$size_class}'>";

    $i = 0;

    foreach ($attachments as $id => $attachment) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= $link;

        if ($captiontag && trim($attachment->post_excerpt)) {
            $output .= "<{$captiontag} class='wp-caption-text gallery-caption'>". wptexturize($attachment->post_excerpt) ."</{$captiontag}>";
        }

        $output .= "</{$itemtag}>";

        if ($columns > 0 && ++$i % $columns == 0) {
            $output .= "<br>";
        }
    }

    $output .= "</div>";

    return $output;
}
add_filter('post_gallery', 'spiral_gallery_output', 10, 2);

/** Remove attributes for uploaded images */
function spiral_remove_image_size_attribute ($html)
{
    return preg_replace('/(width|height)="\d*"/', '', $html);
}
add_filter('post_thumbnail_html', 'spiral_remove_image_size_attribute');
add_filter('image_send_to_editor', 'spiral_remove_image_size_attribute');

/** Show featured images in the dashboard */
function spiral_featured_image_column ($columns)
{
    $columns['featured_image'] = __('Image', 'spiral');
    return $columns;
}
add_filter('manage_posts_columns', 'spiral_featured_image_column', 5);
add_filter('manage_pages_columns', 'spiral_featured_image_column', 5);

function spiral_show_featured_image ($column)
{
    if ('featured_image' === $column) {
        if (function_exists('the_post_thumbnail')) {
            echo the_post_thumbnail('thumbnail');
        }
    }
}
add_action('manage_posts_custom_column', 'spiral_show_featured_image', 5);
add_action('manage_pages_custom_column', 'spiral_show_featured_image', 5);

function spiral_columns_order ($columns)
{
    $columns_new = array();
    $thumbs = $columns['featured_image'];
    unset($columns['featured_image']);

    foreach ($columns as $key => $value) {
        if ('title' == $key) {
            $columns_new['featured_image'] = $thumbs;
        }
        $columns_new[$key] = $value;
    }

    return $columns_new;
}
add_filter('manage_posts_columns', 'spiral_columns_order');
add_filter('manage_pages_columns', 'spiral_columns_order');