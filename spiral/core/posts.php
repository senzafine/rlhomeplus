<?php

/** Add custom post types to the loop */
function spiral_loop_posts ($query)
{
    if ($query->is_main_query()) {
        if ($query->is_home()) {
            $query->set('post_type', ['post']);
        }
    }
}

add_action('pre_get_posts', 'spiral_loop_posts');


/** Add custom post types to the loop */
function spiral_rss_loop_posts ($query)
{
    if ($query->is_feed() && $query->is_main_query() && $query->is_front_page()) {
        $query->set('post_type', ['post']);
    }
}

add_action('pre_get_posts', 'spiral_rss_loop_posts');


/** Get posts */
function spiral_get_posts ($args)
{
    global $post;

    $posts = new WP_Query($args);

    return $posts;
}