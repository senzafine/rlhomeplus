<?php

/** Change url of the login logo */
function spiral_login_url ()
{
    return get_bloginfo('wpurl');
}
add_filter('login_headerurl', 'spiral_login_url');

/** Change the title of the login logo */
function spiral_login_title()
{
    return __('Made by Senzafine and powered by WordPress', 'spiral');
}
add_filter('login_headertitle', 'spiral_login_title');

/** Redirect to home page when logged out */
function spiral_logout_redirect ()
{
    wp_redirect(home_url() . '?login=false');
    exit;
}
add_action('wp_logout', 'spiral_logout_redirect');