<?php

/** Custom post navigation */
function spiral_posts_pagination ()
{
    $arguments  = '';
    $navigation = '';
    $output     = '';

    if ($GLOBALS['wp_query']->max_num_pages > 1) {
        $next_link          = get_previous_posts_link(__('Newer Posts', 'spiral'));
        $link_prev          = get_next_posts_link(__('Older Posts', 'spiral'));
        $screen_reader_text = __('Posts Navigation', 'spiral');

        if ($link_prev) {
            $navigation .= "<div class='posts-prev'>";
            $navigation .= "<svg class='icon'><use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#prev'/></svg>";
            $navigation .= $link_prev;
            $navigation .= "</div>";
        }

        if ($next_link) {
            $navigation .= "<div class='posts-next'>";
            $navigation .= $next_link;
            $navigation .= "<svg class='icon'><use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#next'/></svg>";
            $navigation .= "</div>";
        }

        $output .= "<nav class='pagination'>";
        $output .= "<span class='screen-reader-text'>{$screen_reader_text}</span>";
        $output .= $navigation;
        $output .= "</nav>";
    }

    return $output;
}

/** Multipage post navigation */
function spiral_post_pagination ($arguments = '')
{
    global $page, $numpages, $multipage, $more;

    $defaults = array(
        'link_before'    => '',
        'link_after'     => '',
        'pagelink'       => '%'
    );

    $params             = wp_parse_args($arguments, $defaults);
    $r                  = apply_filters('wp_link_pages_args', $params);
    $screen_reader_text = __('Posts Navigation', 'spiral');
    $link_prev_text     = __('Previous Page', 'spiral');
    $link_next_text     = __('Next Page', 'spiral');
    $output             = '';

    if ($multipage) {
        $output .= "<nav class='pagination'>";
        $output .= "<span class='screen-reader-text'>{$screen_reader_text}</span>";
        $output .= "<div class='post-page-prev'>";
        $prev = $page - 1;

        if ($prev > 0) {
            $link = _wp_link_page($prev) . $link_prev_text .'</a>';
            $output .= "<svg class='icon'><use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#prev'/></svg>";
            $output .= apply_filters('wp_link_pages_link', $link, $prev);
        }

        $next = $page + 1;
        $output .= "</div>";
        $output .= "<div class='post-page-numbers'>";

        for ($i = 1; $i <= $numpages; $i++) {
            $link = str_replace('%', $i, '%');

            if ($i != $page || !$more && 1 == $page) {
                $link = _wp_link_page($i) . $link .'</a>';
            }

            $link = apply_filters('wp_link_pages_link', $link, $i);
            $output .= "<div class='post-page'>{$link}</div>";
        }

        $output .= "</div>";
        $output .= "<div class='post-page-next'>";

        if ($next <= $numpages) {
            $link = _wp_link_page($next) . $link_next_text ."</a>";
            $output .= apply_filters('wp_link_pages_link', $link, $next);
            $output .= "<svg class='icon'><use xlink:href='". get_stylesheet_directory_uri() ."/assets/images/sprite.svg#next'/></svg>";
        }

        $output .= "</div>";
        $output .= "</nav>";
    }

    $output = apply_filters('wp_link_pages', $output, $arguments);

    return $output;
}

/** Check for multipage post */
function spiral_is_paginated ()
{
    global $multipage;
    return 0 !== $multipage;
}
