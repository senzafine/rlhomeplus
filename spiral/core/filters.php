<?php

/** Add <body> classes */
function spiral_body_class (array $classes)
{
    if (is_single() || is_page() && !is_front_page()) {
        if (! in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    if (function_exists('display_sidebar')) {
        if (display_sidebar()) {
            $classes[] = 'sidebar-display';
        }
    }

    return array_filter($classes);
}
add_filter('body_class', 'spiral_body_class');

/** Filter to define the pages that will show the sidebar */
function spiral_sidebar ($display)
{
    static $display;

    isset($display) || $display = in_array(true, [
        is_singular()
    ]);

    return $display;
}
add_filter('spiral/display_sidebar', 'spiral_sidebar');

/** Get the post excerpt */
function spiral_excerpt ($text, $raw_excerpt = '')
{
    global $post;

    $post    = get_post($post);
    $excerpt = $post->post_excerpt;

    if (! $raw_excerpt) {
        $excerpt = apply_filters('the_content', $post->post_content);
        $excerpt = preg_replace("/<img[^>]+\>/i", "", $excerpt);
        $excerpt = preg_replace("/<h(.*?)\b[^>]*>(.*?)<\/h(.*?)>/i", "", $excerpt);
        $excerpt = substr($excerpt, 0, strpos($excerpt, '</p>') + 4);
        $excerpt = substr($excerpt, 0, 150);
        $excerpt = strip_tags($excerpt);
    } else {
        $excerpt = $raw_excerpt;
    }

    return $excerpt;
}
add_filter('wp_trim_excerpt', 'spiral_excerpt', 10, 2);

/** Add featured image to the content */
function spiral_featured_image ($content)
{
    global $post;

    $paged = '';

    if (is_singular() &&
        has_post_thumbnail() &&
        'project' !== get_post_type()) {
        $paged = get_query_var('page') ? get_query_var('page') : false;

        if ($paged == false) {
            $image = "<figure class='entry-thumbnail'>";
            $image .= get_the_post_thumbnail($post->ID, 'featured_image');
            $image .= "</figure>";
            $content = $image . $content;
        }
    }

    return $content;
}
add_filter('the_content', 'spiral_featured_image');

/** Change default avatar */
function spiral_avatar ($avatar_defaults)
{
    $avatar = get_bloginfo('template_directory') .'/images/avatar.svg';
    $avatar_defaults[$avatar] = 'Spiral Avatar';

    return $avatar_defaults;
}
add_filter('avatar_defaults', 'spiral_avatar');

/** Add featured imageto RSS feed */
function spiral_rss_featured_image ($content)
{
    global $post;

    if (has_post_thumbnail($post->ID)) {
        $content = "<p>". get_the_post_thumbnail($post->ID, 'featured_image') ."</p>". get_the_content();
    }

    return $content;
}
add_filter('the_excerpt_rss', 'spiral_rss_featured_image');
add_filter('the_content_feed', 'spiral_rss_featured_image');