<?php

function spiral_post_types ()
{
  register_post_type('slide',
    array(
      'labels' => array(
        'name' => __('Slides', 'spiral'),
        'singular_name' => __('Slide', 'spiral'),
        'add_new_item' => __('Add New Slide', 'spiral'),
        'edit_item' => __('Edit Slide', 'spiral'),
        'new_item' => __('New Slide', 'spiral'),
        'view_item' => __('View Slide', 'spiral'),
        'view_items' => __('View Slides', 'spiral'),
        'search_items' => __('Search Slides', 'spiral'),
        'not_found' => __('No slides found', 'spiral'),
        'not_found_in_trash' => __('No slides found in Trash'),
        'parent_item_colon' => __('Parent Slide', 'spiral'),
        'all_items' => __('All Slides', 'spiral'),
        'archives' => __('Slide Archives', 'spiral'),
        'insert_into_item' => __('Insert into slide', 'spiral'),
        'upload_to_this_item' => __('Upload to this slide', 'spiral'),
        'filter_items_list' => __('Filter slide list', 'spiral'),
        'items_list_navigation' => __('Slides list navigation', 'spiral'),
        'items_list' => __('Slides list')
      ),
      'supports' => array (
        'title',
        'thumbnail'
      ),
      'public' => true,
      'menu_position' => 5
    )
  );
  
  register_post_type('project',
    array(
      'labels' => array(
        'name' => __('Projects', 'spiral'),
        'singular_name' => __('Project', 'spiral'),
        'add_new_item' => __('Add New Project', 'spiral'),
        'edit_item' => __('Edit Project', 'spiral'),
        'new_item' => __('New Project', 'spiral'),
        'view_item' => __('View Project', 'spiral'),
        'view_items' => __('View Projects', 'spiral'),
        'search_items' => __('Search Projects', 'spiral'),
        'not_found' => __('No projects found', 'spiral'),
        'not_found_in_trash' => __('No projects found in Trash'),
        'parent_item_colon' => __('Parent Project', 'spiral'),
        'all_items' => __('All Projects', 'spiral'),
        'archives' => __('Project Archives', 'spiral'),
        'insert_into_item' => __('Insert into project', 'spiral'),
        'upload_to_this_item' => __('Upload to this project', 'spiral'),
        'filter_items_list' => __('Filter project list', 'spiral'),
        'items_list_navigation' => __('Projects list navigation', 'spiral'),
        'items_list' => __('Projects list')
      ),
      'supports' => array(
        'title',
        'editor',
        'revisions',
        'excerpt',
        'thumbnail'
      ),
      'public' => true,
      'menu_position' => 5,
      'has_archive' => true,
      'rewrite' => array(
        'slug' => 'projects'
      )
    )
  );

  register_post_type('testimonial',
    array(
      'labels' => array(
        'name' => __('Testimonials', 'spiral'),
        'singular_name' => __('Testimonial', 'spiral'),
        'add_new_item' => __('Add New Testimonial', 'spiral'),
        'edit_item' => __('Edit Testimonial', 'spiral'),
        'new_item' => __('New Testimonial', 'spiral'),
        'view_item' => __('View Testimonial', 'spiral'),
        'view_items' => __('View Testimonials', 'spiral'),
        'search_items' => __('Search Testimonials', 'spiral'),
        'not_found' => __('No testimonials found', 'spiral'),
        'not_found_in_trash' => __('No testimonials found in Trash'),
        'parent_item_colon' => __('Parent Testimonial', 'spiral'),
        'all_items' => __('All Testimonials', 'spiral'),
        'archives' => __('Testimonial Archives', 'spiral'),
        'insert_into_item' => __('Insert into testimonial', 'spiral'),
        'upload_to_this_item' => __('Upload to this testimonial', 'spiral'),
        'filter_items_list' => __('Filter testimonial list', 'spiral'),
        'items_list_navigation' => __('Testimonials list navigation', 'spiral'),
        'items_list' => __('Testimonials list')
      ),
      'supports' => array (
        'title',
        'editor'
      ),
      'public' => true,
      'exclude_from_search' => true,
      'menu_position' => 5
    )
  );
}
add_action('init', 'spiral_post_types');