/** Define required plugins & files */
const webpack         = require('webpack');
const path            = require('path');
const merge           = require('webpack-merge');
const tasks           = require('./webpack.config.tasks');
const WriteFilePlugin = require('write-file-webpack-plugin');

/** Define paths */
const PATHS = {
  scripts: path.join(__dirname, 'resources/scripts'),
  styles : path.join(__dirname, 'resources/styles'),
  fonts  : path.join(__dirname, 'resources/fonts'),
  images : path.join(__dirname, 'resources/images'),
  sprites: path.join(__dirname, 'resources/images/sprites'),
  node   : path.join(__dirname, 'node_modules'),
  build  : path.join(__dirname, 'spiral/assets'),
  public : '/wp-content/themes/spiral/assets/'
};

/** Global Configuration */
const commonConfig = merge([
  {
    entry: {
      main  : PATHS.scripts + '/main',
      admin : PATHS.scripts + '/admin',
      editor: PATHS.scripts + '/editor',
      login : PATHS.scripts + '/login'
    },

    output: {
      path      : PATHS.build,
      publicPath: PATHS.public
    },

    stats: {
      assets      : true,
      chunks      : false,
      colors      : true,
      entrypoints : true,
      errors      : true,
      errorDetails: true,
      hash        : true,
      warnings    : true
    }
  },

  tasks.showNotifications(),

  tasks.cleanOutput(PATHS.build),

  tasks.loadScripts({
    exclude: /node_modules\/(?!(dom7|swiper)\/).*/
  }),

  tasks.loadSprites({
    include: PATHS.sprites,
    spriteFilename: 'images/sprite.svg'
  }),

  tasks.copyAssets({
    from: PATHS.images,
    to: 'images',
    ignore: [
      'icons/*.*',
      'sprites/*.*',
      'slides/*.*',
      '*.ai',
      '.gitkeep'
    ]
  })
]);

/** Development Configuration */
const developmentConfig = merge([
  {
    output: {
      filename                     : 'scripts/[name].js',
      chunkFilename                : 'scripts/[name].js',
      devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]'
    },

    plugins: [
      new WriteFilePlugin()
    ]
  },

  tasks.devServer({
    host: 'localhost',
    port: 3000,
    proxy: 'localhost:8000',
    files: [
      {
        match: [
          '**/*.php'
        ],
        fn: function(event, file) {
          if (event === 'change') {
            const bs = require('browser-sync').get('bs-webpack-plugin');
            bs.reload();
          }
        }
      }
    ]
  }),

  tasks.extractStyles({
    filename: 'styles/[name].css'
  }),

  tasks.loadImages({
    exclude: [
      PATHS.fonts,
      PATHS.sprites
    ],
    name: 'images/[name].[ext]'
  }),

  tasks.loadFonts({
    include: [
      PATHS.fonts,
      PATHS.node
    ],
    name: 'fonts/[name].[ext]'
  }),

  tasks.sourceMaps({
    type: 'cheap-module-eval-source-map'
  }),

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'development'
  )
]);

/** Production Configuration */
const productionConfig = merge([
  {
    output: {
      filename     : 'scripts/[name].[chunkhash:4].js',
      chunkFilename: 'scripts/[name].[chunkhash:4].js'
    },

    performance: {
      hints: 'warning',
      maxEntrypointSize: 50000,
      maxAssetSize: 450000
    },

    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'initial'
          }
        }
      },
      runtimeChunk: {
        name: 'manifest'
      }
    },

    recordsPath: path.join(__dirname, 'records.json'),

    plugins: [
      new webpack.HashedModuleIdsPlugin()
    ]
  },

  tasks.minifyScripts(),

  tasks.extractStyles({
    filename: 'styles/[name].[chunkhash:4].css'
  }),

  tasks.minifyStyles(),

  tasks.loadImages({
    exclude: [
      PATHS.fonts,
      PATHS.sprites
    ],
    name: 'images/[name].[hash:4].[ext]'
  }),

  tasks.loadFonts({
    include: [
      PATHS.fonts,
      PATHS.node
    ],
    name: 'fonts/[name].[hash:4].[ext]'
  }),

  tasks.sourceMaps({
    type: 'source-map'
  }),

  tasks.assetsManifest({
    path: PATHS.build
  }),

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'production'
  )
]);

/** Merge global & defined environment configurations */
module.exports = mode => {
  if (mode === 'production') {
    return merge(commonConfig, productionConfig, { mode });
  }

  return merge(commonConfig, developmentConfig, { mode });
};
