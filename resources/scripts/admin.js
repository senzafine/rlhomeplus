/** HMR configuration */
if (module.hot) {
  module.hot.accept();
}

if (module.hot) {
  const hotEmitter = require('webpack/hot/emitter');
  const DEAD_CSS_TIMEOUT = 2000;

  hotEmitter.on('webpackHotUpdate', function(currentHash) {
    document.querySelectorAll('link[href][rel=stylesheet]').forEach((link) => {
      const nextStyleHref = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`);
      const newLink = link.cloneNode();

      newLink.href = nextStyleHref;

      link.parentNode.appendChild(newLink);

      setTimeout(() => {
        link.parentNode.removeChild(link);
      }, DEAD_CSS_TIMEOUT);
    });
  });
}

/** Import style */
import '../styles/admin.styl';

/* Define the address of the WordPress AJAX function file */
// let ajax_url = '/wp/wp-admin/admin-ajax.php';

/** jQuery */
import $ from 'jquery';

/* Setup functions */
$(document).on('click', '#project-before-button', function (e) {
  e.preventDefault();
  
  let image_preview = $('#project-before-preview');
  let image_id      = $('#project-before-id');
  
  spiral_upload_image(image_preview, image_id);
});

$(document).on('click', '#project-after-button', function (e) {
  e.preventDefault();
  
  let image_preview = $('#project-after-preview');
  let image_id      = $('#project-after-id');
  
  spiral_upload_image(image_preview, image_id);
});

function spiral_upload_image (image_preview, image_id) {
  let mediaUploader;
  
  if (mediaUploader) {
    mediaUploader.open();
    return;
  }
  
  mediaUploader = wp.media.frames.file_frame = wp.media({
    title: 'Select or upload an image',
    button: {
      text: 'Select image'
    },
    multiple: false
  });
  
  mediaUploader.on('select', function (){
    let attachment = mediaUploader.state().get('selection').first().toJSON();
    
    console.log(attachment);
    
    $(image_preview).attr({src: attachment.sizes.thumbnail.url});
    $(image_id).val(attachment.id);
    
    console.log('Image selected.');
  });
  
  mediaUploader.open();
}