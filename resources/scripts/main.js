/** HMR configuration */
if (module.hot) {
  module.hot.accept();
}

if (module.hot) {
  const hotEmitter = require('webpack/hot/emitter');
  const DEAD_CSS_TIMEOUT = 2000;

  hotEmitter.on('webpackHotUpdate', function(currentHash) {
    document.querySelectorAll('link[href][rel=stylesheet]').forEach((link) => {
      const nextStyleHref = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`);
      const newLink = link.cloneNode();

      newLink.href = nextStyleHref;

      link.parentNode.appendChild(newLink);

      setTimeout(() => {
        link.parentNode.removeChild(link);
      }, DEAD_CSS_TIMEOUT);
    });
  });
}

/** Import normalize.css */
import 'normalize.css';

/** Import style */
import '../styles/main.styl';

/** Define sprites folder and files */
const sprites = require.context('../images/sprites', true, /\.svg$/);

sprites.keys().forEach(function (key) {
  sprites(key);
});

/** Lightgallery */
import 'lightgallery.js/dist/css/lightgallery.css';
import 'lightgallery.js';
import 'lg-zoom.js';
import 'lg-fullscreen.js';

let projectPhotos = document.querySelectorAll('.project-photos');

[].forEach.call (projectPhotos, function (gallery) {
  let lightbox = lightGallery(gallery, {
    mode: 'lg-fade',
    download: false,
    selector: 'figure a'
  });

  window.onload = lightbox;
});

let galleryPhotos = document.querySelectorAll('.gallery');

[].forEach.call (galleryPhotos, function (gallery) {
  let lightbox = lightGallery(gallery, {
    mode: 'lg-fade',
    download: false,
    selector: 'figure a'
  });

  window.onload = lightbox;
});

/** Swiper */
import 'swiper/dist/css/swiper.css';
import Swiper from 'swiper/dist/js/swiper.js';

const frontSwiper = new Swiper('.swiper-frontpage', {
  effect: 'fade',
  centeredSlides: true,
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  pagination: {
    el: '.swiper-pagination'
  },
  /*
  autoplay: {
    delay: 2500,
    disableOnInteraction: false
  },
  */
  fadeEffect: {
    crossFade: true
  },
  keyboard: {
    enabled: true
  }
});

window.onload = frontSwiper;

const testimonialSwiper = new Swiper('.swiper-testimonial', {
  effect: 'slide',
  centeredSlides: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  autoplay: {
    delay: 7500,
    disableOnInteraction: true
  },
  fadeEffect: {
    crossFade: true
  }
});

window.onload = testimonialSwiper;

/** Zenscroll */
import zenscroll from 'zenscroll';

const home = document.querySelector('#home');
zenscroll.to(home);

/** Window scroll */
window.onscroll = function () {
  let header = document.querySelector('.header-main');
  let headerHeight = header.offsetHeight;
  let menu = document.querySelector('.menu-main');
  let menuHeight = menu.offsetHeight;
  let logo = document.querySelector('.brand-small');
  let fixedClass = 'fixed';

  if (window.pageYOffset > (headerHeight - menuHeight)) {
    logo.classList.add(fixedClass);
    menu.classList.add(fixedClass);
  } else {
    logo.classList.remove(fixedClass);
    menu.classList.remove(fixedClass);
  }
};

/** Show & hide the admin login bar */
let logo = document.querySelector('.brand');
let admin = document.querySelector('.admin-bar');
let showClass = 'show';

logo.addEventListener('click', function (event) {
  event.preventDefault();
  if (admin.classList.contains(showClass)) {
    admin.classList.remove(showClass);
  } else {
    admin.classList.add(showClass);
  }
});

/** Quote form pop up */
let quoteClose = document.querySelector('.quote-close');
let quoteOpen = document.querySelector('.button-menu a');
let quotePopup = document.querySelector('.quote-popup');

quoteOpen.addEventListener('click', function (event) {
  event.preventDefault();
  quotePopup.classList.add(showClass);
});

quoteClose.addEventListener('click', function (event) {
  event.preventDefault();

  if (quotePopup.classList.contains(showClass)) {
    quotePopup.classList.remove(showClass);
  } else {
    quotePopup.classList.add(showClass);
  }
});

/** Show a message before submitting form */
let quoteForm = document.querySelector('.quote-form form');
let quoteSubmit = document.querySelector('.quote-form input[type="submit"]');
let quoteMessage = document.querySelector('.quote-message');
let errorClass = 'error';
let errorCount = 0;

quoteSubmit.addEventListener('click', function (event) {
  event.preventDefault();

  let requiredFields = document.querySelectorAll('.ninja-forms-req');
  
  requiredFields.forEach( function(requiredField) {
    if (requiredField.value == '') {
      requiredField.classList.add(errorClass);
      errorCount++;
    } else {
      requiredField.classList.remove(errorClass);
      errorCount = 0;
    }
  });

  if (errorCount == 0) {
    quoteMessage.classList.add(showClass);
    setTimeout(() => quoteForm.submit(), 5000);
  }
});

/** Mail */
window.onload = function () {
  AJAXform('contact-form', 'contact-submit', 'contact-message', 'post');
};

function AJAXform(formID, buttonID, resultID, formMethod = 'post') {
  var selectForm    = document.getElementById(formID);
  var selectButton  = document.getElementById(buttonID);
  var selectResult  = document.getElementById(resultID);
  var formAction    = document.getElementById(formID).getAttribute('action');
  var formInputs    = document.getElementById(formID).querySelectorAll('input');
  var formTextareas = document.getElementById(formID).querySelectorAll('textarea');
  
  function XMLhttp() {
    var httpRequest = new XMLHttpRequest();
    var formData = new FormData();
    
    for (var i=0; i < formInputs.length; i++) {
      formData.append(formInputs[i].name, formInputs[i].value);
    }

    for (var i=0; i < formTextareas.length; i++) {
      formData.append(formTextareas[i].name, formTextareas[i].value);
    }
    
    httpRequest.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        selectResult.classList.add(showClass);
        selectResult.innerHTML = this.responseText;
      }
    };
    
    httpRequest.open(formMethod, formAction);
    httpRequest.send(formData);
  }
  
  selectButton.onclick = function () {
    XMLhttp();
  }
  
  selectForm.onsubmit = function() {
    return false;
  }
}