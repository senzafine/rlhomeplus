/** Define constants and plugins */
const webpack                     = require('webpack');
const path                        = require('path');
const WebpackNotifierPlugin       = require('webpack-notifier');
const CleanWebpackPlugin          = require('clean-webpack-plugin');
const BrowserSyncPlugin           = require('browser-sync-webpack-plugin');
const UglifyWebpackPlugin         = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin        = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin     = require('optimize-css-assets-webpack-plugin');
const SpriteLoaderPlugin          = require('svg-sprite-loader/plugin');
const CopyWebpackPlugin           = require('copy-webpack-plugin');
const WebpackAssetsManifest       = require('webpack-assets-manifest');

/** Show notifications */
exports.showNotifications = () => {
  return {
    plugins: [
      new WebpackNotifierPlugin({
        title: 'Spiral',
        contentImage: path.join(__dirname, 'resources/images/icon.png')
      })
    ]
  };
};

/** Empty the output directory */
exports.cleanOutput = (path) => {
  let cleanPaths = [
    path
  ];

  let cleanOptions = {
    root: __dirname,
    verbose: false,
    exclude: ['.gitkeep']
  };

  return {
    plugins: [
      new CleanWebpackPlugin(cleanPaths, cleanOptions)
    ]
  };
};

/** Process javascript files */
exports.loadScripts = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        include,
        exclude,
        use: 'babel-loader'
      }
    ]
  }
});

/** Minify javascript files */
exports.minifyScripts = () => ({
  optimization: {
    minimizer: [
      new UglifyWebpackPlugin({
        sourceMap: true
      })
    ]
  }
});

/** Process stylesheets */
exports.loadStyles = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(styl|css)$/,
        include,
        exclude,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: 'inline',
              plugins: () => [
                require('autoprefixer')()
              ]
            }
          },
          'stylus-loader'
        ]
      }
    ]
  }
});

/** Extract stylesheets from bundle */
exports.extractStyles = ({ include, exclude, filename, chunkFilename }) => ({
  module: {
    rules: [
      {
        test: /\.(styl|css)$/,
        include,
        exclude,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: 'inline',
              plugins: () => [
                require('autoprefixer')()
              ]
            }
          },
          {
            loader: 'stylus-loader'
          }
        ]
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename,
      chunkFilename
    })
  ]
});

/** Minify stylesheet files */
exports.minifyStyles = () => ({
  plugins: [
    new OptimizeCSSAssetsPlugin({
      cssProcessor: require('cssnano'),
      canPrint: true,
      cssProcessorOptions: {
        discardComments: {
          removeAll: true
        },
        safe: true
      }
    })
  ]
});

/** Process image files */
exports.loadImages = ({ include, exclude, name } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(svg|png|jpe?g|gif)$/,
        include,
        exclude,
        use: [
          {
            loader: 'file-loader',
            options: {
              name
            }
          },

          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true
              },
              gifsicle: {
                interlaced: false
              },
              optipng: {
                optimizationLevel: 7
              },
              pngquant: {
                quality: '75-90',
                speed: 3
              }
            }
          }
        ]
      }
    ]
  }
});

/** Combine svg files inside a given directory into one sprite */
exports.loadSprites = ({ include, exclude, spriteFilename }) => ({
  module: {
    rules: [
      {
        test: /\.svg$/,
        include,
        exclude,

        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true,
              spriteFilename
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new SpriteLoaderPlugin()
  ]
});

/** Process font files */
exports.loadFonts = ({ include, exclude, name } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(svg|eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        include,
        exclude,
        use: [
          {
            loader: 'file-loader',
            options: {
              name
            }
          }
        ]
      }
    ]
  }
});

/** Create source map */
exports.sourceMaps = ({ type }) => ({
  devtool: type
});

/** Copy assets from a given directory to the output directory */
exports.copyAssets = ({ from, to, ignore }) => ({
  plugins: [
    new CopyWebpackPlugin([{ from, to, ignore }])
  ]
});

/** Create a manifest file for WordPress */
exports.assetsManifest = () => ({
  plugins: [
    new WebpackAssetsManifest({
      output: 'assets.json',
      writeToDisk: true,
      replacer: function(key, value) {
        if (typeof value === 'string') {
          return value;
        }

        const manifest = value;

        Object.keys(manifest).forEach((src) => {
          const sourcePath = path.basename(path.dirname(src));
          const targetPath = path.basename(path.dirname(manifest[src]));

          if (sourcePath === targetPath) {
            return;
          }

          manifest[`${targetPath}/${src}`] = manifest[src];
          delete manifest[src];
        });

        return manifest;
      }
    })
  ]
});

/** Environment Tasks */
exports.setFreeVariable = (key, value) => {
  const env = {};

  env[key] = JSON.stringify(value);

  return {
    plugins: [
      new webpack.DefinePlugin(env)
    ]
  };
};

/** Configure BrowserSync */
exports.devServer = ({ host, port, proxy, files } = {}) => ({
  devServer: {
    stats: 'errors-only',
    overlay: true,
    hot: true
  },
  plugins: [
    new BrowserSyncPlugin(
      {
        host,
        port,
        proxy,
        files,
        open: false
      },
      {
        reload: false,
        injectCss: true
      }
    ),

    new webpack.HotModuleReplacementPlugin(),

    new webpack.NamedModulesPlugin()
  ]
});
