module.exports = {
  extends: 'eslint:recommended',
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  parser: "babel-eslint",
  parserOptions: {
    sourceType: 'module',
    allowImportExportEverywhere: true
  },
  plugins: ['import'],
  rules: {
    indent: ['error', 2],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    'comma-dangle': ['error', 'never'],
    'linebreak-style': ['error', 'unix'],
    'no-unused-vars': ['warn'],
    'no-undef': 0,
    'no-console': 0
  }
};
